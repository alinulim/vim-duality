# vim-duality

Monochromatic dark color scheme for vim inspired by [preto.vim][1] and [bow-wob][2]

vim-duality offers a grayscale foreground text over a dark background.

## Installation

Clone (or download) the repo 

```bash 
git clone https://gitlab.com/alinulim/vim-duality.git
```

Copy the `colors/duality.vim` file in your `$VIMRUNTIME/colors/` directory.

* system-wide

```bash
/usr/share/vim/vimXX/colors/
# where XX is the installed version of vim
```

Create (or edit) the /etc/vimrc file and add the line:

```VimL
colorscheme duality
```

* user-specific

```bash
~/.vim/colors/
```

Create (or edit) your ~/.vimrc file and add the line:

```VimL
colorscheme duality
```

## License

Licensed under the MIT License. See the [LICENSE][3] file for the full license information.

[//]: # (Reference-style links. See - https://daringfireball.net/projects/markdown/syntax#link)

   [1]: <https://github.com/ewilazarus/preto>
   [2]: <https://github.com/p7g/vim-bow-wob> 
   [3]: <https://gitlab.com/alinulim/vim-duality/-/blob/master/LICENSE>

   
