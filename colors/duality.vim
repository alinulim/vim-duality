" ==============================================================================
" File:        duality.vim
" Description: Minimal monochromatic dark vim color scheme inspired in presto.vim
"			   and bow-wob.vim
" Maintainer:  Alistair Endulheim
" ==============================================================================

highlight clear
if exists("syntax_on")
  syntax reset
endif

set background=dark
let g:colors_name = "duality"

highlight Comment guifg=# guibg= gui= ctermfg= ctermbg= cterm=
highlight Constant guifg=# guibg= gui= ctermfg= ctermbg= cterm=
highlight String guifg=# guibg= gui= ctermfg= ctermbg= cterm=
highlight Character guifg=# guibg= gui= ctermfg= ctermbg= cterm=
highlight Number guifg=# guibg= gui= ctermfg= ctermbg= cterm=
highlight Boolean guifg=# guibg= gui= ctermfg= ctermbg= cterm=
highlight Float guifg=# guibg= gui= ctermfg= ctermbg= cterm=
highlight Identifier guifg=# guibg= gui= ctermfg= ctermbg= cterm=
highlight Function guifg=# guibg= gui= ctermfg= ctermbg= cterm=
highlight Statement guifg=# guibg= gui= ctermfg= ctermbg= cterm=
highlight Conditional guifg=# guibg= gui= ctermfg= ctermbg= cterm=
highlight Repeat guifg=# guibg= gui= ctermfg= ctermbg= cterm=
highlight Label guifg=# guibg= gui= ctermfg= ctermbg= cterm=
highlight Operator guifg=# guibg= gui= ctermfg= ctermbg= cterm=
highlight Keyword guifg=# guibg= gui= ctermfg= ctermbg= cterm=
highlight Exception guifg=# guibg= gui= ctermfg= ctermbg= cterm=
highlight PreProc guifg=# guibg= gui= ctermfg= ctermbg= cterm=
highlight Include guifg=# guibg= gui= ctermfg= ctermbg= cterm=
highlight Define guifg=# guibg= gui= ctermfg= ctermbg= cterm=
highlight Macro guifg=# guibg= gui= ctermfg= ctermbg= cterm=
highlight PreCondit guifg=# guibg= gui= ctermfg= ctermbg= cterm=
highlight Type guifg=# guibg= gui= ctermfg= ctermbg= cterm=
highlight StorageClass guifg=# guibg= gui= ctermfg= ctermbg= cterm=
highlight Structure guifg=# guibg= gui= ctermfg= ctermbg= cterm=
highlight Typedef guifg=# guibg= gui= ctermfg= ctermbg= cterm=
highlight Special guifg=# guibg= gui= ctermfg= ctermbg= cterm=
highlight SpecialChar guifg=# guibg= gui= ctermfg= ctermbg= cterm=
highlight Tag guifg=# guibg= gui= ctermfg= ctermbg= cterm=
highlight Delimiter guifg=# guibg= gui= ctermfg= ctermbg= cterm=
highlight SpecialComment guifg=# guibg= gui= ctermfg= ctermbg= cterm=
highlight Debug guifg=# guibg= gui= ctermfg= ctermbg= cterm=
highlight Underlined guifg=# guibg= gui= ctermfg= ctermbg= cterm=
highlight Ignore guifg=# guibg= gui= ctermfg= ctermbg= cterm=
highlight Error guifg=# guibg= gui= ctermfg= ctermbg= cterm=
highlight Todo guifg=# guibg= gui= ctermfg= ctermbg= cterm=

" highlight groups
highlight ColorColumn guifg=# guibg= gui= ctermfg= ctermbg= cterm=
highlight Conceal guifg=# guibg= gui= ctermfg= ctermbg= cterm=
highlight Cursor guifg=# guibg= gui= ctermfg= ctermbg= cterm=
highlight lCursor guifg=# guibg= gui= ctermfg= ctermbg= cterm=
highlight CursorIM guifg=# guibg= gui= ctermfg= ctermbg= cterm=
highlight CursorColumn guifg=# guibg= gui= ctermfg= ctermbg= cterm=
highlight CursorLine guifg=# guibg= gui= ctermfg= ctermbg= cterm=
highlight Directory guifg=# guibg= gui= ctermfg= ctermbg= cterm=
highlight DiffAdd guifg=# guibg= gui= ctermfg= ctermbg= cterm=
highlight DiffChange guifg=# guibg= gui= ctermfg= ctermbg= cterm=
highlight DiffDelete guifg=# guibg= gui= ctermfg= ctermbg= cterm=
highlight DiffText guifg=# guibg= gui= ctermfg= ctermbg= cterm=
highlight EndOfBuffer guifg=# guibg= gui= ctermfg= ctermbg= cterm=
highlight ErrorMsg guifg=# guibg= gui= ctermfg= ctermbg= cterm=
highlight VertSplit guifg=# guibg= gui= ctermfg= ctermbg= cterm=
highlight Folded guifg=# guibg= gui= ctermfg= ctermbg= cterm=
highlight FoldColumn guifg=# guibg= gui= ctermfg= ctermbg= cterm=
highlight SignColumn guifg=# guibg= gui= ctermfg= ctermbg= cterm=
highlight IncSearch guifg=# guibg= gui= ctermfg= ctermbg= cterm=
highlight LineNr guifg=# guibg= gui= ctermfg= ctermbg= cterm=
highlight LineNrAbove guifg=# guibg= gui= ctermfg= ctermbg= cterm=
highlight LineNrBelow guifg=# guibg= gui= ctermfg= ctermbg= cterm=
highlight CursorLineNr guifg=# guibg= gui= ctermfg= ctermbg= cterm=
highlight MatchParen guifg=# guibg= gui= ctermfg= ctermbg= cterm=
highlight ModeMsg guifg=# guibg= gui= ctermfg= ctermbg= cterm=
highlight MoreMsg guifg=# guibg= gui= ctermfg= ctermbg= cterm=
highlight NonText guifg=# guibg= gui= ctermfg= ctermbg= cterm=
highlight Normal guifg=# guibg= gui= ctermfg= ctermbg= cterm=
highlight Pmenu guifg=# guibg= gui= ctermfg= ctermbg= cterm=
highlight PmenuSel guifg=# guibg= gui= ctermfg= ctermbg= cterm=
highlight PmenuSbar guifg=# guibg= gui= ctermfg= ctermbg= cterm=
highlight PmenuThumb guifg=# guibg= gui= ctermfg= ctermbg= cterm=
highlight Question guifg=# guibg= gui= ctermfg= ctermbg= cterm=
highlight QuickFixLine guifg=# guibg= gui= ctermfg= ctermbg= cterm=
highlight Search guifg=# guibg= gui= ctermfg= ctermbg= cterm=
highlight SpecialKey guifg=# guibg= gui= ctermfg= ctermbg= cterm=
highlight SpellBad guifg=# guibg= gui= ctermfg= ctermbg= cterm=
highlight SpellCap guifg=# guibg= gui= ctermfg= ctermbg= cterm=
highlight SpellLocal guifg=# guibg= gui= ctermfg= ctermbg= cterm=
highlight SpellRare guifg=# guibg= gui= ctermfg= ctermbg= cterm=
highlight StatusLine guifg=# guibg= gui= ctermfg= ctermbg= cterm=
highlight StatusLineNC guifg=# guibg= gui= ctermfg= ctermbg= cterm=
highlight StatusLineTerm guifg=# guibg= gui= ctermfg= ctermbg= cterm=
highlight StatusLineTermNC guifg=# guibg= gui= ctermfg= ctermbg= cterm=
highlight TabLine guifg=# guibg= gui= ctermfg= ctermbg= cterm=
highlight TabLineFill guifg=# guibg= gui= ctermfg= ctermbg= cterm=
highlight TabLineSel guifg=# guibg= gui= ctermfg= ctermbg= cterm=
highlight Terminal guifg=# guibg= gui= ctermfg= ctermbg= cterm=
highlight Title guifg=# guibg= gui= ctermfg= ctermbg= cterm=
highlight Visual guifg=# guibg= gui= ctermfg= ctermbg= cterm=
highlight VisualNOS guifg=# guibg= gui= ctermfg= ctermbg= cterm=
highlight WarningMsg guifg=# guibg= gui= ctermfg= ctermbg= cterm=
highlight WildMenu guifg=# guibg= gui= ctermfg= ctermbg= cterm=
